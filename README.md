# Elecciones[^1]

[^1]: Punto 1 del parcial 2020-10: <https://sophia.javeriana.edu.co/programacion/sites/default/files/pdfParciales/2020-10-Introducci%C3%B3nProgramaci%C3%B3n-2.pdf>

En un país de Sudamérica, se cuenta con la información de la votación en cada
uno de los estados que conforman el país. La información se registra indicando
el código del partido ganador y la cantidad de votos que obtuvo ese partido en
el estado. La información se registra de tal manera que los primeros dos datos
corresponden al estado 0, los siguientes dos datos al estado 1 y así
sucesivamente:

```text
 Estado00           | Estado01            | ... | EstadoXX            |
Ganador | No. Votos | Ganador | No. Votos | ... | Ganador | No. Votos |
      4 |      2560 |       1 |      8510 | ... |       3 |      6582 |
```

Se tiene un arreglo con los nombres de los estados, el código del estado es la
posición que ocupa en el arreglo.

```text
Calcedonia | Angostura | Fundadores | Terrano | ... | Niza |
 0         | 1         | 2          | 3       | ....| n    |
```

Se tiene un arreglo con los nombres de los partidos, el código del partido es
la posición que ocupa en el arreglo.


```text
Verde | Azul | Patriota | Libertad | Popular | ... |
 0    | 1    | 2        | 3        | 4       | ... |
```

Con la información disponible y teniendo cuidado al definir todos los
parámetros que necesitan las funciones para hacer su trabajo:

- Construya una función que genere un nuevo arreglo donde para cada partido
  calcule la cantidad total de votos obtenidos en los estados donde ganó.
  Considere que el nuevo arreglo debe estar disponible para otras funciones.

## Refinamiento 1

1. Función recibe arreglo con partidos ganadores por estado con cantidad de
   votos, tamaño de arreglo de ganadores y arreglo para almacenar totales por
   partido inicializdo en ceros.
   1. Recorro arreglo de ganadores desde `0` hasta tamaño menos `2`:
      1. Sumo a la posición del partido los votos obtenidos.

## Refinamiento 2

<div class="center">

```mermaid
graph TD
    ini("Inicio")-->setI["i = 0"]
    setI-->iCond{"i < n - 1"}
    iCond--"No"-->fin("Fin")
    iCond--"Sí"-->setDeri["tot[gana[i]] += gana[i + 1]"]
    setDeri-->iInc["i += 2"]
    iInc-->iCond
```
</div>

#### Entradas:

- `gana[]`: arreglo que contiene cantidad de votos de partido ganador por
  estado.
- `n`: tamaño del arreglo `gana[]`.
- `tot[]`: arreglo sobre el que se guarda el total de votos de partidos
  ganadores.

#### Salidas:

- Ninguna (referencia).

#### Pseudo-código:

1. Para `i` desde `0`; hasta `i` menor a `n - 1`; incremento `i` en `2`:
   1. Sumo a `tot[]` en posición `gana[i]` el valor de `gana[i + 1]`.

# Referencias
