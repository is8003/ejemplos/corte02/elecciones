#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ESTADOS 10
#define PARTIDOS 5
#define MAX_VOTOS 1000

// Prototipos
void iniGana(unsigned int list[], unsigned int n, unsigned int numPar, unsigned int maxVot);
void impList(const unsigned int list[], unsigned int n);
void totalPart(unsigned int gana[], unsigned int n, unsigned int tot[]);
void impEst(unsigned int n);
void impPa(unsigned int n);

int main(void){
	unsigned int ganadores[ESTADOS * 2];
	unsigned int total[PARTIDOS] = {0};

	srand(time(NULL));

	iniGana(ganadores, ESTADOS * 2, PARTIDOS, MAX_VOTOS);
	puts("Ganadores por estado:");
	impList(ganadores, ESTADOS * 2);
	impEst(ESTADOS * 2);
	puts("");

	totalPart(ganadores, ESTADOS * 2, total);
	puts("Totales por partido:");
	impList(total, PARTIDOS);
	impPa(PARTIDOS);
	puts("");

}

void totalPart(unsigned int gana[], unsigned int n, unsigned int tot[]){
	for (size_t i = 0; i < n - 1; i += 2){
		tot[gana[i]] += gana[i + 1];
	}
}

// Inicializa listado de ganadores por estado
void iniGana(unsigned int list[], unsigned int n, unsigned int numPar, unsigned int maxVot){
	for (size_t i = 0; i < n - 1; i += 2){
			list[i] = rand() % numPar;
			list[i+1] = rand() % maxVot;
	}
}

void impList(const unsigned int list[], unsigned int n){
	for (size_t i = 0; i < n; i++){
		printf("%4u|", list[i]);
	}

	puts("");
}

void impEst(unsigned int n){
	for (unsigned int i = 0; i < n - 1; i +=2){
		printf("Est. %04u|", i/2);
	}

	puts("");
}

void impPa(unsigned int n){
	for (unsigned int i = 0; i < n; i++){
		printf("Pa%02u|", i);
	}

	puts("");
}
